# README #

Dilatua has two components, a server side and a client side for smartphones. The server side is built using Node.js and Docker and the client side using Corona SDK for Android/iOs phones.

### What is this repository for? ###

Creates a form on the client to submit image/text and stores the content in MongoDb. Images are stored as Base64.

### How do I get set up? ###

* Server:
* npm install
* npm start
* Make sure MongoDB is installed on the machine, and run it with mongod.

* Client(Android only):
* Open dilatua folder with Corona SDK
* Build, copy the .apk on your devices and install, then run it.