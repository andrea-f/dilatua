/********** Authentication ***********/
// Interaction information here: https://wiki.youview.co.uk/display/YVCDI/Watch+Later+List+Integration
var express = require('express');
var helpers = require('express-helpers');
//var request = require('request');
var rp = require('request-promise');
var HOME_DIR = "/code/app";

var view = express.Router();
req_counter = 0;
// Gets device PUID, needed to get the client id token
view.get('/show_submissions', function(req, res, next) {
	console.log("getting submissions...")
	User.find({}, null, {sort:{created:1}}, function(err, users){	
		console.log("Showing users..1")
		//console.log(users[0].comment)
		res.render(HOME_DIR+'/views/user_saved.ejs', { 	
			title: 'Saved User',
			users: users
		});
	});
	
});

module.exports = view;
/*
<td>
	    	<img src="data:image/png;base64,<%= users[i].photod %>" alt="Red dot" />
			</td>
*/