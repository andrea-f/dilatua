/********** Authentication ***********/
// Interaction information here: https://wiki.youview.co.uk/display/YVCDI/Watch+Later+List+Integration
var express = require('express');
var helpers = require('express-helpers');
//var request = require('request');
var rp = require('request-promise');
var HOME_DIR = "/code/app";

var submit = express.Router();
submit.user = {};
req_counter = 0;
// Gets device PUID, needed to get the client id token
submit.post('/verify', function(req, res, next) {
	/*
		User input:
		- photo XOR comment XOR video
		- email (optional)
	*/
	console.log(req.headers)
	//console.log(req.body)
	submit.getInputValidation(req).then(function(user) {
			//console.log(user);
			console.log("Received user submission: " + user.create)
			//console.log("Save user comment: " + user.comment)
			//console.log("Gradimento: "+user.mi_piace)
			if (user.create) {
				submit.saveInDb(user);
				res.sendStatus(200);
			}			
	});
	
});

submit.saveInDb = function(user_input) {
	new User({
		photo : user_input.photo,
		comment : user_input.comment,
		email : user_input.email,
		mi_piace: user_input.mi_piace
	})
	.save(function(err, user) {
		if(!err) {
			
			console.log("Gradimento: "+user.mi_piace);
			console.log("Commento: "+user.comment);
			console.log("Saved user...")
		} else {
			console.log(err);
		}
	});
}

submit.validateComment = function (comment) {
	// Check if comment is safe text 
	return true;
}

submit.validateImage = function (image) {
	// Check if image is base64 encoded valid image string.
	return true;
}

submit.validateEmail = function (email) {
	// Check if email is a valid format email address
	return false
}

submit.getInputValidation = function (req) {
	// Instantiate scoped user
	return new Promise(function(resolve){
		var user = {};
		user.create = false;

		// VALIDATE COMMENT
		if (req.body.commento) {
			var is_valid_comment = submit.validateComment(req.body.commento); // returns either true or false
		
			if (is_valid_comment) {
				user.comment = decodeURI(req.body.commento);
				user.create = true;
			}
		}

		// VALIDATE PHOTO
		if (req.body.photo) {
			var is_valid_image = submit.validateImage(req.body.photo); // returns either true or false
		
			if (is_valid_image) {
				user.photo = req.body.photo;
				user.create = true;
			}
		}

		// VALIDATE EMAIL
		if (req.body.email) {
			var is_valid_image = submit.validateEmail(req.body.email); // returns either true or false
		
			if (is_valid_image) {
				user.email = req.body.email;
				user.create = true;
			}
		}

		user.mi_piace = req.body.mi_piace

		resolve(user);
	});
}

module.exports = submit;


/********** END Authentication ***********/
