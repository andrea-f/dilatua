/* 
"nodemon /code/app/server.js"
GLOBALS 
*/
require("./database.js");

express = require('express');
helpers = require('express-helpers');
bodyParser = require('body-parser');

//request = require('request');
HOME_DIR = "/code/app";
var mongoose = require('mongoose');


/* GLOBALS */

ObjectID = require('mongodb').ObjectID;
User = mongoose.model('user');

var Promise = require('bluebird');
var submit = Promise.promisifyAll(require(HOME_DIR+'/routes/submit'));
var view = Promise.promisifyAll(require(HOME_DIR+'/routes/view'));

//var auth = require(HOME_DIR+'/routes/auth')

var app = express();
//var browserify = require('browserify-middleware');
app.use(bodyParser({limit: '50mb'}));
helpers(app);
app.use(express.static(__dirname + '/public'));

//app.get(HOME_DIR+'/app.js', browserify(HOME_DIR+'/client/main.js'));

app.get('/', function(req, res){
  //res.render(HOME_DIR+'/views/index.ejs');
  console.log("Home requested!")
});

app.use('/submit', submit);
app.use('/view', view);

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Node listening at http://%s:%s', host, port);
});

module.exports = server