var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var User = new Schema({
    email : String,
    photo : String,
    video: String,
    comment: String,
    place: String,
    mi_piace: String,
    created: {
        type: Date,
        default: Date.now
    }
});
mongoose.model('user', User);
mongoose.connect('mongodb://userDatabase5:27017/users');