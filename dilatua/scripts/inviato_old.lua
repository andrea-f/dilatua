inviato = {}
local mediabuttons = require("scripts.mediabuttons")
local home = require("scripts.homepage")
local widget = require( "widget" )

-- BOTTONE GRAZIE
inviato.showDisplayObjects = function ()
	display.setDefault( "background", 1, 1, 1 )
	local options = {
	    --parent = textGroup,
	    text = "Grazie\nper il tuo contributo:",     
	    x = (display.contentCenterX),
	    y = (display.contentCenterY - 200),
	    width = 320,     --required for multi-line and alignment
	    font = "Calibri",   
	    fontSize = 30,
	    align = "center"  --new alignment parameter
	}

	inviato.heading = display.newText(options)
	inviato.heading:setFillColor( 0, 0, 0 )
    if (home.user.commento) then
        -- COMMENTO UTENTE
    	inviato.testo = display.newText( home.user.commento, (display.contentCenterX), (display.contentCenterY-100), "Calibri", 18 )
		inviato.testo:setFillColor( 0, 0, 0 ) -- metto il colore rosso

    end

    -- IMMAGINE SELEZIONATA
    if (home.user.photo) then
    	local screen_adjustment = 0.1

        inviato.photo = display.newImage( home.user.photo, system.TemporaryDirectory, true )
	    --local h = (screen_adjustment*inviato.photo.height)/100--(display.contentHeight/ (inviato.photo.height)))
	    --inviato.photo:scale( h,h)
	    inviato.photo.xScale = ((screen_adjustment  * (display.contentHeight))/(display.contentHeight))
	    inviato.photo.yScale = inviato.photo.xScale
	    inviato.photo.x = display.contentCenterX--display.contentWidth /2
	    inviato.photo.y = display.contentCenterY

    end

end

local function networkListener( event )

    if ( event.isError ) then
        print( "Network error!" )
    elseif ( event.phase == "ended" ) then
    	print("Response arrived...")
        print ( "RESPONSE: " .. event.response )
        inviato.sent = display.newText( "INVIATA!", (display.contentCenterX+100), (display.contentCenterY + 250), "Calibri", 20 )
		inviato.sent:setFillColor( 0.15, 0.686, 0.46,1 ) -- metto il colore rosso
	
    end
end

local function makeNetworkRequest1(payload) 
	print("Making request to API")
	local MultipartFormData = require("scripts.class_MultipartFormData")
 
	local multipart = MultipartFormData.new()
	multipart:addHeader("Content-Type", payload.content_type)
	--print(payload.user.commento)
	multipart:addField("comment",payload.user.commento)
	multipart:addFile("myfile", system.pathForFile( "myImage.jpg", system.TemporaryDirectory ), "image/jpeg", "myfile.jpg")
	 
	local params = {}

	params.body = multipart:getBody() -- Must call getBody() first!
	print(params)
	params.headers = multipart:getHeaders() -- Headers not valid until getBody() is called.
	network.request( payload.url, payload.method, networkListener, params )
end

local function makeNetworkRequest2(payload) 
	 
	local mime = require("mime")

	local json = require("json")

	local path = system.pathForFile( payload.user.photo, system.TemporaryDirectory )
    local fileHandle = io.open( path, "rb" )
    local imageb64 = mime.b64(fileHandle:read( "*a" ))
    --print(mime.b64(fileHandle:read( "*a" )))
    print(path)

	local URL = "http://192.168.1.74:3000/submit/verify/" ..
	      mime.b64(payload.user.commento) --.. "/" .. imageb64

	network.request( URL, "POST", networkListener ) 
	io.close( fileHandle )
    --if fileHandle then
      --          params.progress = "download";
               
	--network.upload( 
	  --  "http://127.0.0.1/restapi.php", 
	   -- "POST", 
	    --networkListener, 
	    --"object.json", 
	    --system.DocumentsDirectory, 
	    --"application/json"
	--)

end


function urlencode(str)
      if (str) then
          str = string.gsub (str, "\n", "\r\n")
          str = string.gsub (str, "([^%w ])",
          function ( c ) return string.format ("%%%02X", string.byte( c )) end)
          str = string.gsub (str, " ", "+")
      end
      return str    
end

local function makeNetworkRequest(payload)
	local json = require "json"
	local mime = require("mime")

	print ("test")
	local path = system.pathForFile( payload.user.photo, system.TemporaryDirectory )
    local fileHandle = io.open( path, "rb" )
    local imageb64 = mime.b64(fileHandle:read( "*a" ))
    print(payload.user.commento)
	sendInfo = {["commento"] = payload.user.commento, ["photo"] = imageb64}

	--print(sendInfo)

	local headers = {
	    ["Content-Type"] = "application/json",--"application/x-www-form-urlencoded", 
	    ["Accept-Language"] = "en-US",
	}

	local params = {}
	params.headers = headers
	params.body = json.encode( sendInfo )

	--print( "params.body: "..params.body )

	network.request( payload.url, "POST", networkListener, params)
end

local function handleButtonEventInvia( event )

    if ( "ended" == event.phase ) then
        print( "Button testo was pressed and released11111" )
        --inviato.removeHomeObjects()
        payload = {}
        payload.user = home.user
        payload.url = "http://192.168.99.100:3000/submit/verify"
        payload.method = "POST"
        payload.content_type ="application/json"   -- "multipart/form-data"
        
        -- NETWORK REQUEST MANAGER
        makeNetworkRequest(payload)
        --inviato.showTextEntry()

    end
end


inviato.invia = widget.newButton(
    {
        onEvent = handleButtonEventInvia,
        emboss = false,
        width = 80,
        height = 40,
        shape = "roundedRect",
        cornerRadius = 20,
        fillColor = { default={1, 0.58, 0.16,1} , over={1,0.1,0.7,0.4} },
        --strokeColor = { default={0,1,1,1}, over={0.8,0.8,1,1} },
        --strokeColor = { default={0,0,0,1}, over={0.8,0.8,1,1} },
        labelColor = { default={ 1, 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
        strokeWidth = 2
       
    }
)

-- BOTTONE INVIA
inviato.invia.x = display.contentCenterX
inviato.invia.y = display.contentCenterY+((display.contentHeight/2))
inviato.invia:setLabel( "INVIA" )

local function inputListener( event )

    if ( event.phase == "began" ) then
        -- user begins editing defaultField
        print( event.text )

    elseif ( event.phase == "ended" or event.phase == "submitted" ) then
        -- do something with defaultField text
        print( event.target.text )
        home.user.email = event.target.text
        --network.request( url, method, listener [, params] )
        --192.168.99.100:3000



    elseif ( event.phase == "editing" ) then
        print( event.newCharacters )
        print( event.oldText )
        print( event.startPosition )
        print( event.text )
    end
end
-- CAMPO EMAIL 
inviato.email = native.newTextField( (display.contentCenterX), (display.contentCenterY + (display.contentHeight/2.7)), 260, 28 )
inviato.email:setTextColor( 0.8, 0.8, 0.8 )
inviato.email.hasBackground = true
inviato.email.text = "indirizzo@email.com"
inviato.email.align = "center"
inviato.email:addEventListener( "userInput", inputListener )
--inviato.email:setStrokeColor({ default={0,0,0,1} , over={1,0.1,0.7,0.4} })
local function convertRGB(r, g, b)
   assert(r and g and b and r <= 255 and r >= 0 and g <= 255 and g >= 0 and b <= 255 and b >= 0, "You must pass all 3 RGB values within a range of 0-255");
   return r/255, g/255, b/255;
end



return inviato