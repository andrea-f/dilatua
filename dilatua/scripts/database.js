var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Company = new Schema({
    companyName : String,
    companyAccessGroup : String,
    publisherId: String
});

var Application = new Schema({
    name: String,
    url: String,
    applicationId: String, 
    company: Company
})

var User = new Schema({
    firstName : String,
    lastName : String,
    email : String,
    company : Company
});

var Device = new Schema({
	manufacturer : String,
	model : String,
	variant : String,
	assetNo : String,
	duid : String,
	company : Company
});

mongoose.model('company', Company);
mongoose.model('user', User);
mongoose.model('device', Device);
mongoose.model('application', Application);
mongoose.connect('mongodb://db:27017/developer-register');