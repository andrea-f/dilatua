inviato = {}
local mediabuttons = require("scripts.mediabuttons")
local home = require("scripts.homepage")
local widget = require( "widget" )

local function networkListener( event )

    if ( event.isError ) then
        print( "Network error!" )
    elseif ( event.phase == "ended" ) then
        print("Response arrived...")
        print ( "RESPONSE: " .. event.response )

        -- SCRITTA INVIATA
        inviato.sent = display.newText( "INVIATA!", (display.contentCenterX), (display.contentCenterY + (display.contentHeight/2.7)), "Calibri", 20 )
        inviato.sent:setFillColor( 0.15, 0.686, 0.46,1 ) -- metto il colore verde
        inviato.sending.isVisible = false
        inviato.sent.isVisible = true
    elseif ((event.phase == "began") or (event.phase == "progress")) then
        print ( "Request sent... " )        
        inviato.sending = display.newText( "INVIANDO...", (display.contentCenterX), (display.contentCenterY + (display.contentHeight/2.7)), "Calibri", 20 )
        inviato.sending:setFillColor( 0.15, 0.686, 0.46,1 ) -- metto il colore verde
        inviato.sending.isVisible = true
    end
end


function urlencode(str)
      if (str) then
          str = string.gsub (str, "\n", "\r\n")
          str = string.gsub (str, "([^%w ])",
          function ( c ) return string.format ("%%%02X", string.byte( c )) end)
          str = string.gsub (str, " ", "+")
      end
      return str    
end

local function makeNetworkRequest(payload)
    local json = require "json"
    local mime = require("mime")

    local path = system.pathForFile( payload.user.photo, system.TemporaryDirectory )
    local fileHandle = io.open( path, "rb" )
    local imageb64 = mime.b64(fileHandle:read( "*a" ))
    --print(payload.user.commento)
    sendInfo = {["commento"] = payload.user.commento, ["photo"] = imageb64, ["mi_piace"] = payload.user.mi_piace, ["email"] = payload.user.email}
    --print(sendInfo)
    local headers = {
        ["Content-Type"] = "application/json",--"application/x-www-form-urlencoded", 
        ["Accept-Language"] = "en-US",
    }

    local params = {}
    params.headers = headers
    params.body = json.encode( sendInfo )
    --print( "params.body: "..params.body )
    network.request( payload.url, "POST", networkListener, params)
end

local function handleButtonEventInvia( event )

    if ( "ended" == event.phase ) then
        --print( "Button testo was pressed and released11111" )
        --inviato.removeHomeObjects()
        payload = {}
        payload.user = home.user
        print(home.user.mi_piace)
        payload.url = "http://192.168.1.72:3000/submit/verify"
        payload.method = "POST"
        payload.content_type ="application/json"   -- "multipart/form-data"
        
        -- NETWORK REQUEST MANAGER
        makeNetworkRequest(payload)
        --inviato.showTextEntry()

    end
end



local function inputListener( event )

    if ( event.phase == "began" ) then
        -- user begins editing defaultField
        print( event.text )

    elseif ( event.phase == "ended" or event.phase == "submitted" ) then
        -- do something with defaultField text
        print( event.target.text )
        home.user.email = event.target.text
        --network.request( url, method, listener [, params] )
        --192.168.99.100:3000



    elseif ( event.phase == "editing" ) then
        print( event.newCharacters )
        print( event.oldText )
        print( event.startPosition )
        print( event.text )
    end
end



--inviato.email:setStrokeColor({ default={0,0,0,1} , over={1,0.1,0.7,0.4} })
local function convertRGB(r, g, b)
   assert(r and g and b and r <= 255 and r >= 0 and g <= 255 and g >= 0 and b <= 255 and b >= 0, "You must pass all 3 RGB values within a range of 0-255");
   return r/255, g/255, b/255;
end


-- SHOW DISPLAY AND HIDE PREVIOUS
inviato.showDisplayObjects = function ()
    --mediabuttons.removeHomeObjects()
	display.setDefault( "background", 1, 1, 1 )
    status.currentPage = "invia"
    -- SCRITTA GRAZIE
	local options = {
	    --parent = textGroup,
	    text = "Grazie\nper il tuo contributo!",     
	    x = (display.contentCenterX),
	    y = (display.contentCenterY - 100),
	    width = 320,     --required for multi-line and alignment
	    font = "Calibri",   
	    fontSize = 30,
	    align = "center"  --new alignment parameter
	}
	inviato.heading_h1 = display.newText(options)
	inviato.heading_h1:setFillColor( 0, 0, 0 )


    -- SCRITTA INSERISCI MAIL PER FAVORE
    local options = {
        --parent = textGroup,
        text = "Per poterci inviare il tuo contributo\ninserisci la tua email.",     
        x = (display.contentCenterX),
        y = (display.contentCenterY)-(display.contentHeight/12),
        width = 320,     --required for multi-line and alignment
        font = "Calibri",   
        fontSize = 15,
        align = "center"  --new alignment parameter
    }
    inviato.heading = display.newText(options)
    inviato.heading:setFillColor(convertRGB(105, 105, 105))


    -- RETTANGOLO EMAIL
    inviato.rect = display.newRect( (display.contentCenterX), (display.contentCenterY+(display.contentHeight/5)), 280, 40   )
    inviato.rect.strokeWidth = 0
    inviato.rect:setFillColor( 0.827,0.827,0.827 ,1)

    -- CAMPO EMAIL 
    inviato.email = native.newTextField( (display.contentCenterX), (display.contentCenterY+(display.contentHeight/5)), 260, 28 )
    inviato.email:setTextColor( 0.8, 0.8, 0.8 )
    inviato.email.hasBackground = true
    inviato.email.text = "indirizzo@email.com"
    inviato.email.align = "center"
    inviato.email:addEventListener( "userInput", inputListener )


    -- BOTTONE INVIA
    inviato.invia = widget.newButton(
        {
            onEvent = handleButtonEventInvia,
            emboss = false,
            width = 200,
            height = 40,
            shape = "roundedRect",
            cornerRadius = 20,
            fillColor = { default={1, 0.58, 0.16,1} , over={1,0.1,0.7,0.4} },
            --strokeColor = { default={0,1,1,1}, over={0.8,0.8,1,1} },
            --strokeColor = { default={0,0,0,1}, over={0.8,0.8,1,1} },
            labelColor = { default={ 1, 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            strokeWidth = 2
           
        }
    )
    inviato.invia.x = display.contentCenterX
    inviato.invia.y = display.contentCenterY+((display.contentHeight/2))
    inviato.invia:setLabel( "INVIA" )

end

inviato.removePageObjects = function ()
    inviato.email.isVisible = false
    inviato.rect.isVisible = false
    inviato.invia.isVisible = false
    inviato.heading.isVisible = false
    inviato.heading_h1.isVisible = false
end


return inviato