-- scriptTest.lua (in your scripts directory)
local M = {}
M.user = {}

local function hidePageObjects()
	-- body
	M.myTextObject.isVisible = false
	M.myTextObject1.isVisible = false
	M.myTextObject2.isVisible = false
	M.myTextObject3.isVisible = false
end

local function show()
    print("show home")
    display.setDefault( "background", 1, 1, 1 )

    M.myTextObject = display.newText( "Inviaci", 160, 160, "Calibri", 35 )
	M.myTextObject:setFillColor( 0, 0, 0 ) -- metto il colore rosso
	
	M.myTextObject1 = display.newText( "il tuo contributo", 160, 190, "Calibri", 35 )
	M.myTextObject1:setFillColor( 0, 0, 0 ) -- metto il colore rosso
	
	
	M.myTextObject2 = display.newText( "Inviaci un testo, una foto o un video su", 160, 230, "Calibri", 15 )
	M.myTextObject2:setFillColor( M.convertRGB(105, 105, 105) ) -- metto il colore rosso
	
	M.myTextObject3 = display.newText( "cosa ti piace o non ti piace della tua città", 160, 250, "Calibri", 15 )
	M.myTextObject3:setFillColor( M.convertRGB(105, 105, 105) ) -- metto il colore rosso
end

M.convertRGB = function(r, g, b)
   assert(r and g and b and r <= 255 and r >= 0 and g <= 255 and g >= 0 and b <= 255 and b >= 0, "You must pass all 3 RGB values within a range of 0-255");
   return r/255, g/255, b/255;
end






M.show = show

return M