local widget = require( "widget" )
local mediapage = require("scripts.mediapage")
local home = require("scripts.homepage")

local buttons = {}
local function removeHomeObjects()
    display.remove(home.myTextObject)
    display.remove(home.myTextObject1)
    display.remove(home.myTextObject2)
    display.remove(home.myTextObject3)
    display.remove(buttons.button1)
    display.remove(buttons.button2)
end


-- Function to handle button events
local function handleButtonEventMiPiace( event )

    if ( "ended" == event.phase ) then
        print( "Button MI PIACE was pressed and released" )
        home.user.mi_piace = "Mi piace"
        removeHomeObjects()

        mediapage.show()
    end
end

local function handleButtonEventNonMiPiace( event )

    if ( "ended" == event.phase ) then
        print( "Button was pressed and released" )
        home.user.mi_piace = "Non mi piace";
        removeHomeObjects()

        mediapage.show()

    end
end

-- Create the widget
buttons.displayHomeObjects = function ()
    buttons.button1 = widget.newButton(
        {
            label = "NON MI PIACE",
            onEvent = handleButtonEventNonMiPiace,
            emboss = false,
            -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 200,
            height = 40,
            cornerRadius = 20,
            fillColor = { default={1,0,0,1}, over={1,0.1,0.7,0.4} },
            --strokeColor = { default={0,1,1,1}, over={0.8,0.8,1,1} },

            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            strokeWidth = 4
        }
    )


    -- Center the button
    buttons.button1.x = display.contentCenterX
    buttons.button1.y = display.contentCenterY + 140

    buttons.button2 = widget.newButton(
        {
            label = "MI PIACE",
            onEvent = handleButtonEventMiPiace,
            emboss = false,
            -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 200,
            height = 40,
            cornerRadius = 20,
            fillColor = { default={0.15, 0.686, 0.46, 1 }, over={1,0.1,0.7,0.4} },
            --strokeColor = { default={0,1,1,1}, over={0.8,0.8,1,1} },

            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            strokeWidth = 4
        }
    )

    -- Center the button
    buttons.button2.x = display.contentCenterX
    buttons.button2.y = display.contentCenterY + 80
end

return buttons