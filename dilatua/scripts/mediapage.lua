
local media_object = {}

local function show()
    print("show media")
    local mediabuttons = require("scripts.mediabuttons")
    mediabuttons.showHomeObjects()
end



media_object.convertRGB = function(r, g, b)
   assert(r and g and b and r <= 255 and r >= 0 and g <= 255 and g >= 0 and b <= 255 and b >= 0, "You must pass all 3 RGB values witmedia_objectin a range of 0-255");
   return r/255, g/255, b/255;
end

media_object.show = show

return media_object


