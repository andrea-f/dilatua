local widget = require( "widget" )
local home = require("scripts.homepage")
--local keyHandler = require("scripts.keys")

local mediabuttons = {}
mediabuttons.convertRGB = function (r, g, b)
   assert(r and g and b and r <= 255 and r >= 0 and g <= 255 and g >= 0 and b <= 255 and b >= 0, "You must pass all 3 RGB values within a range of 0-255");
   return r/255, g/255, b/255;
end
display.setDefault( "background", mediabuttons.convertRGB(139, 0, 0))

mediabuttons.testoObject = {}
mediabuttons.removeHomeObjects = function ()
    --display.remove(mediabuttons.testo)
    --display.remove(mediabuttons.immagine)
    --display.remove(mediabuttons.video)
    print("Hiding display objects...")
    mediabuttons.testoObject.isVisible = false
    --if pcall(mediabuttons.testo) then
    print("Hiding testo object")
    mediabuttons.testo.isVisible = false
    display.remove(mediabuttons.testo)
    mediabuttons.testo = nil
    --end
    --if pcall(mediabuttons.immagine == true) then
    print("Hiding immagine object")
    mediabuttons.immagine.isVisible = false
    display.remove(mediabuttons.immagine)
    mediabuttons.immagine = nil
    --end
    --if pcall(mediabuttons.video == true) then
    print("Hiding video object") 
    mediabuttons.video.isVisible = false
    display.remove(mediabuttons.video)
    mediabuttons.video = nil    
    --end
    --if (mediabuttons.testoObject) then
    --if unexpected_condition then
    if pcall(mediabuttons.testoObject) then
      -- no errors while running `foo'
      
      mediabuttons.testoObject = nil
      display.remove(mediabuttons.testoObject)
    else
      -- `foo' raised an error: take appropriate actions
      print("mediabuttons.testoObject doesnt exist yet.")
    end

    
    
    if (mediabuttons.invia) then
        mediabuttons.invia.isVisible = false
    end
end

mediabuttons.showHomeObjects = function ()
    display.setDefault( "background", mediabuttons.convertRGB(139, 0, 0))
    local screen_adjustment = 0.5
    status.currentPage = "scelta_input"
    print("setting currentPage...to scelta_input")
    --keyHandler.currentPage = "scelta_input"

    -------------------
    -- BOTTONE CONFERMA
    -------------------
    mediabuttons.invia = widget.newButton(
    {
        onEvent = mediabuttons.handleButtonEventInvia,
        emboss = false,
        shape = "roundedRect",
        width = 200,
        height = 40,
        cornerRadius = 20,
        fillColor = { default={1,0,0,0}, over={1,0.1,0.7,0.4} },
        strokeColor = { default={1,1,1,1}, over={0.8,0.8,1,1} },

        labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
        strokeWidth = 2
    })
    mediabuttons.invia.x = display.contentCenterX
    mediabuttons.invia.y = display.contentCenterY+((display.contentHeight/2))
    mediabuttons.invia:setLabel( "CONFERMA" )
    mediabuttons.invia.isVisible = false


    -- BOTTONE TESTO
    mediabuttons.testo = widget.newButton(
    {
        onEvent = mediabuttons.handleButtonEventTesto,
        emboss = false,
        defaultFile = "testo.png",
        overFile = "testo.png",
        width = (display.contentWidth / 1.6),
        height= (display.contentHeight / 2.2)
    })
    mediabuttons.testo.xScale = ((screen_adjustment  * (display.contentWidth / 1.8))/(display.contentWidth / 1.8)) 
    mediabuttons.testo.yScale = mediabuttons.testo.xScale
    mediabuttons.testo.x = display.contentCenterX
    mediabuttons.testo.y = display.contentCenterY - 150


    --BOTTONE IMMAGINI
    mediabuttons.immagine = widget.newButton(
        {
            onEvent = mediabuttons.handleButtonEventImmagine,
            emboss = false,
            defaultFile = "immagine.png",
            overFile = "immagine.png",
            width = (display.contentWidth / 1.6),
            height= (display.contentHeight / 2.2)
        }
    )
    mediabuttons.immagine.xScale = ((screen_adjustment  * (display.contentWidth / 1.8))/(display.contentWidth / 1.8)) 
    mediabuttons.immagine.yScale = mediabuttons.immagine.xScale
    -- Center the button
    mediabuttons.immagine.x = display.contentCenterX
    mediabuttons.immagine.y = display.contentCenterY


    -- BOTTONE VIDEO
    mediabuttons.video = widget.newButton(
    {
        onEvent = mediabuttons.handleButtonEventVideo,
        emboss = false,
        defaultFile = "video.png",
        overFile = "video.png",
        width = (display.contentWidth / 1.6),
        height= (display.contentHeight / 2.2)
    })
    mediabuttons.video.xScale = ((screen_adjustment  * (display.contentWidth / 1.8))/(display.contentWidth / 1.8)) 
    mediabuttons.video.yScale = mediabuttons.video.xScale
    mediabuttons.video.x = display.contentCenterX
    mediabuttons.video.y = display.contentCenterY + 150

    mediabuttons.testo.isVisible = true
    mediabuttons.immagine.isVisible = true
    mediabuttons.video.isVisible = true

    local can_submit = false
    if (home.user.commento) then
        mediabuttons.testoObject = display.newText( "OK", 260, 80, "Calibri", 20 )
        mediabuttons.testoObject:setFillColor( 1, 1, 1 ) -- metto il colore rosso
        can_submit = true
    end

    if (home.user.photo) then
        mediabuttons.immagineObject = display.newText( "OK", 260, 230, "Calibri", 20 )
        mediabuttons.immagineObject:setFillColor( 1, 1, 1 ) -- metto il colore rosso
        can_submit = true
    end

    if (can_submit == true) then
        mediabuttons.invia.isVisible = true
    end

end

-- Function to handle button events
mediabuttons.handleButtonEventTesto = function( event )

    if ( "ended" == event.phase ) then
        print( "Button testo was pressed and released" )
        mediabuttons.removeHomeObjects()
        usertext = require("scripts.textentry")
        usertext.showDisplayObjects()
        --mediabuttons.showTextEntry()

    end
end

mediabuttons.handleButtonEventImmagine = function ( event )

    if ( "ended" == event.phase ) then
        print( "Button Immagine was pressed and released" )
        mediabuttons.removeHomeObjects()
        immagineentry = require("scripts.immagineentry")
    end
end

-- Function to handle button events
mediabuttons.handleButtonEventVideo = function ( event )

    if ( "ended" == event.phase ) then
        print( "Button video was pressed and released" )
    end
end

mediabuttons.handleButtonEventInvia = function ( event )

    if ( "ended" == event.phase ) then
        print( "Button Invia was pressed and released" )
        inviato = require("scripts.inviato")
        mediabuttons.removeHomeObjects()
        inviato.showDisplayObjects()
    end
end

return mediabuttons