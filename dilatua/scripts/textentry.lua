local usertext = {}
local widget = require( "widget" )
local home = require("scripts.homepage")
local mediabuttons = require("scripts.mediabuttons")
local keys = require("scripts.keys")
local function handleButtonEventSalva( event )

    if ( "ended" == event.phase ) then
        print( "Button was pressed and released" )
        --myTextObject2 = display.newText( usertext.textBox.text, 160, 350, "Calibri", 22 )
        --myTextObject2:setFillColor( usertext.convertRGB(105, 105, 105) ) -- metto il colore rosso
        home.user.commento = usertext.textBox.text
        --myTextObject3 = display.newText( home.user.mi_piace, 160, 370, "Calibri", 22 )
        --myTextObject3:setFillColor( usertext.convertRGB(105, 105, 105) ) -- metto il colore rosso
        usertext.hideDisplayObjects()
    end
end

local function handleButtonEventBack( event )
    usertext.hideDisplayObjects()
    mediabuttons.showHomeObjects()
end

local function showCoordinates(name, x, y, width, height)
    print("-------------------START----------------------")
    print("----------------------------------------------")

    if name then
        print("name: " .. name)
    end
    if (x and y) then
        print("x: " .. x .. " y: " .. y)
    end
    if (width and height) then
        print("width: " .. width .. " height: " .. height)
    end
    
    print("Display width: " .. display.contentWidth .. " display height: " .. display.contentHeight)
    print("----------------------------------------------")
    print("--------------------END-----------------------")


end

local function inputListener( event )
    if event.phase == "began" then
        -- user begins editing textBox
        print( event.text )

    elseif event.phase == "ended" then
        -- do something with textBox text
        usertext.testo = event.target.text 
        

    elseif event.phase == "editing" then
        print( event.newCharacters )
        print( event.oldText )
        print( event.startPosition )
        print( event.text )
        usertext.testo = event.text 
    end
end
-- native.newTextBox( centerX, centerY, width, height )
usertext.showDisplayObjects = function () 
    status.currentPage = "testo"
    display.setDefault( "background", 1, 1, 1 )
    -- TOP HEADER CON IMMAGINE
    local screen_adjustment = 0.3
    usertext.rect = display.newRect( (display.contentCenterX), 0, display.contentWidth, display.contentHeight/3.5   )
    usertext.rect.strokeWidth = 0
    usertext.rect:setFillColor( 0.54,0,0 ,1)
    
    --- LOGO
    usertext.logo = display.newImage( "testo_logo.png")
    usertext.logo.xScale = ((screen_adjustment  * usertext.logo.contentWidth)/usertext.logo.contentWidth) 
    usertext.logo.yScale = usertext.logo.xScale
    usertext.logo.x = display.contentCenterX
    usertext.logo.y = (display.contentCenterY - (display.contentHeight/2.2))

    -- BOTTONE BACK
    usertext.back = widget.newButton(
        {
            onEvent = handleButtonEventBack,
            emboss = false,
            defaultFile = "back.png",
            overFile = "back.png",
            width = (display.contentWidth / 9),
            height= (display.contentHeight / 7 )
        }
    )
    usertext.back.xScale = ((screen_adjustment  * (display.contentWidth / 10))/(display.contentWidth / 10)) 
    usertext.back.yScale = usertext.back.xScale
    usertext.back.x = display.contentCenterX - (display.contentCenterX/1.1)
    usertext.back.y = (display.contentCenterY - (display.contentHeight/2.2))
    usertext.back.isVisible = true

    -- BOTTONE SALVA
    usertext.salva = widget.newButton(
    {
        onEvent = handleButtonEventSalva,
        emboss = false,
        width = 80,
        height = 40,
        shape = "roundedRect",
        cornerRadius = 20,
        fillColor = { default={1, 0.58, 0.16,1} , over={1,0.1,0.7,0.4} },
        strokeColor = { default={1,1,1,1}, over={0.8,0.8,1,1} },
        labelColor = { default={ 1, 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
        strokeWidth = 2
    })
    usertext.salva.x = display.contentCenterX
    usertext.salva.y = display.contentCenterY+((display.contentHeight/2))--((display.contentHeight/2)*0.01))
    usertext.salva:setLabel( "SALVA" )
    usertext.salva.isVisible = true
    showCoordinates("SALVA" ,usertext.salva.x,usertext.salva.y)

    
    usertext.textBox.isVisible = true
    showCoordinates("TEXTBOX",usertext.textBox.x,usertext.textBox.y, usertext.textBox.width, usertext.textBox.height)

end

-- TEXTBOX
usertext.textBox = native.newTextBox(display.contentCenterX, (display.contentCenterY), (display.contentWidth-(display.contentWidth*0.05)), (display.contentHeight-(display.contentHeight*.3)) )
usertext.textBox.text = "Aggiungi un commento alla situazione"
usertext.textBox.isEditable = true
usertext.textBox:addEventListener( "userInput", inputListener )

usertext.hideDisplayObjects = function () 
    usertext.salva.isVisible = false
    usertext.textBox.isVisible = false
    usertext.logo.isVisible = false
    usertext.rect.isVisible = false
    usertext.back.isVisible = false
    mediabuttons.showHomeObjects()

end

return usertext
