immagineentry = {}
local mediabuttons = require("scripts.mediabuttons")
local home = require("scripts.homepage")


immagineentry.onPhotoComplete = function ( event )
   if ( event.completed ) then
      local screen_adjustment = 0.2
      local name = "myImage.jpg"
      local photo = display.newImage( name, system.TemporaryDirectory, true )

      --local photo = event.target
      --local h = display.contentHeight/ (photo.height)
      --local w = display.contentWidth/photo.width
      --physics.addBody( photo, { density=3.0, friction=0.5, bounce=0.3 } )
      --photo:scale( h,h)
      --photo.xScale = ((screen_adjustment  * photo.contentWidth)/photo.contentWidth)
      --photo.yScale = photo.xScale
      --photo.x = display.contentWidth / 1.5
      --photo.y = display.contentHeight / 2
      home.user.photo = name
      photo.isVisible = false
      display.remove(photo)
      photo = nil

      mediabuttons.showHomeObjects()
      --photo:translate( 0,0)
      --print( "photo w,h = " .. photo.width .. "," .. photo.height )
      --print( "scale photo w,h = " .. w .. "," .. h )

      --media.selectPhoto( { mediaSource = media.PhotoLibrary, listener = onPhotoComplete })

   end
end

if ( media.hasSource( media.Camera ) ) then
   media.capturePhoto( {
   listener = immagineentry.onPhotoComplete, 
   destination = {
      baseDir = system.TemporaryDirectory,
      filename = "myImage.jpg",
      type = "image"
   }
} )
else
	
   native.showAlert( "Corona", "This device does not have a camera.", { "OK" } )
end
return immagineentry