    --main.lua

    -- Called when a key event has been received
--	local function onKeyEvent( event )
	    -- Print which key was pressed down/up
--	    local message = "Key '" .. event.keyName .. "' was pressed " .. event.phase
--	    print( message )

	    -- If the "back" key was pressed on Android or Windows Phone, prevent it from backing out of the app
--	    if ( event.keyName == "back" ) then
--	        local platformName = system.getInfo( "platformName" )
--	        if ( platformName == "Android" ) or ( platformName == "WinPhone" ) then
--	            return true
--	        end
--	    end

	    -- IMPORTANT! Return false to indicate that this app is NOT overriding the received key
	    -- This lets the operating system execute its default handling of the key
--	    return false

--	end
--handle the Android hardware back and menu buttons
local home = require("scripts.homepage")
local button = require("scripts.button")

status = {}
status.currentPage = "home"
local function onKeyEvent(event)
 
    local phase = event.phase
    local keyName = event.keyName
    print("key event: " .. keyName)
    
    -- Listening for B as well so can test Android Back with B key
    --(( platformName == "Android" ) or ( platformName == "WinPhone" )))
    print("currentPage == " .. status.currentPage)
    if ("back" == keyName and phase == "down") or (("b" == keyName and phase == "down" and system.getInfo("environment") == "simulator")) then
 		local mediabuttons = require("scripts.mediabuttons")
        if ( "scelta_input" == status.currentPage ) then
 			mediabuttons.removeHomeObjects()
 			status.currentPage = "home"
 			home.show()
        	button.displayHomeObjects()
            print("currentPage == home")

        	return true -- prevent default behaviour
    	elseif ( "testo" == status.currentPage ) then
    		usertext.hideDisplayObjects()
        	mediabuttons.showHomeObjects()
            print("currentPage == testo")
        	return true
        elseif ("invia" == status.currentPage) then
        	inviato.removePageObjects()
        	mediabuttons.showHomeObjects()
            print("currentPage == invia")
        	return true
      	elseif ( "home" == status.currentPage ) then
      		print("currentPage == home")

        	return false
      	end
    end
end
 
Runtime:addEventListener( "key", onKeyEvent ) 
	-- Add the key event listener

home.show()
button.displayHomeObjects()